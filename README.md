# 说明

根目录下已经存在一个jar，改后缀后即可运行，这个jar是添加了如下配置打包的，没有添加注解可解析到参数名

```
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.11.0</version> <!-- Replace with the version you need -->
				<configuration>
					<compilerArgs>
						<arg>-parameters</arg>
					</compilerArgs>
				</configuration>
			</plugin>

```

需要测试的盆友可以去掉以上配置，直接命令行打包测试以下

如果去掉上面的配置打包后，无法正确解析方法参数名，无法正常请求测试地址

测试地址：

http://localhost:8080/demo/get?id=232&name=a