package com.example2.demo2.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo")
public class DemoController {

	@GetMapping("get")
	public Map<String, String> get(String id, String name) {
		Map<String, String> map = new HashMap<>();
		map.put("id", id);
		map.put("name", name);
		return map;
	}

}
